﻿/**
 * Copyright (c) CRM Partners Pty Ltd - All rights reserved.
 * Unauthorized copying or redistribution of this file, in part
 * or in whole, via any medium is strictly prohibited.
 * 
 * The contents of this file are proprietary and confidential.
 */

using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Crm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Workflow;

namespace Crmp.Core.Workflows
{
    public class SequenceGenerator : CodeActivity
    {
        [RequiredArgument]
        [Input("Enforced Debugging")]
        public InArgument<bool> EnforcedDebugging { get; set; }

        private static readonly object SyncLock = new object();

        protected override void Execute(CodeActivityContext activity)
        {
            IWorkflowContext context = activity.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory factory = activity.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            ITracingService tracing = activity.GetExtension<ITracingService>();

            tracing.Trace("Entered workflow code");
            Entity entity = null;
            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] != null)
            {
                tracing.Trace("Target entity exists");
                entity = (Entity)context.InputParameters["Target"];
                entity = service.Retrieve(entity.LogicalName, entity.Id, new ColumnSet(true));
            }
            else
            {
                tracing.Trace("Using PrimaryEntity details");
                string entityname = context.PrimaryEntityName;
                Guid entityid = context.PrimaryEntityId;
                entity = service.Retrieve(context.PrimaryEntityName, context.PrimaryEntityId, new ColumnSet(true));
            }

            if (entity != null)
            {
                // find active sequence generator records for this entity type.
                QueryExpression q0 = new QueryExpression("crmp_sequencegenerator");
                FilterExpression f0 = new FilterExpression();
                f0.FilterOperator = LogicalOperator.And;
                f0.Conditions.Add(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
                f0.Conditions.Add(new ConditionExpression("crmp_primaryentityname", ConditionOperator.Equal, entity.LogicalName));
                q0.Criteria = f0;

                EntityCollection sequences = service.RetrieveMultiple(q0);
                tracing.Trace("Found " + sequences.Entities.Count + " sequence generators");
                for (int i = 0; i < sequences.Entities.Count; i++)
                {
                    bool skip = false;
                    Entity sequence = service.Retrieve(sequences[i].LogicalName, sequences[i].Id, new ColumnSet(true));
                    tracing.Trace("Processing generator " + i);
                    string attribute = sequence.Contains("crmp_primaryattributename") ? (string)sequence["crmp_primaryattributename"] : "";
                    tracing.Trace("Attribute name: " + attribute);
                    if (attribute != "")
                    {
                        string checkattribute = sequence.Contains("crmp_secondaryattributename") ? (string)sequence["crmp_secondaryattributename"] : "";
                        string checkvalue = sequence.Contains("crmp_secondaryattributevalue") ? (string)sequence["crmp_secondaryattributevalue"] : "";
                        tracing.Trace("Check attribute: " + checkattribute);
                        if (checkattribute != "")
                        {
                            tracing.Trace("Need to qualify the sequence generation");
                            if (entity.Contains(checkattribute))
                            {
                                tracing.Trace("Target contains " + checkattribute);
                                string val = "";
                                var attr = entity.Attributes[checkattribute];
                                string atype = attr.GetType().Name;
                                switch (atype)
                                {
                                    case "OptionSetValue":
                                        val = ((Microsoft.Xrm.Sdk.OptionSetValue)attr).Value.ToString();
                                        break;
                                    case "Boolean":
                                        val = attr.ToString();
                                        break;
                                    case "EntityReference":
                                        val = ((EntityReference)attr).Name;
                                        break;
                                    default:
                                        val = attribute.ToString();
                                        break;
                                }

                                if (val != checkvalue)
                                {
                                    skip = true;    // qualification failed, don't generate
                                }
                            }
                            else
                            {
                                tracing.Trace("Target does not contain the check attribute");
                                skip = true;
                            }
                        }

                        tracing.Trace("Skip: " + skip);
                        if (skip == false)
                        {
                            try
                            {
                                tracing.Trace("In try block");
                                lock (SyncLock)
                                {
                                    tracing.Trace("In synclock");
                                    string prefix = sequence.Contains("crmp_prefix") ? (string)sequence["crmp_prefix"] : "";
                                    string suffix = sequence.Contains("crmp_postfix") ? (string)sequence["crmp_postfix"] : "";
                                    string numfmt = sequence.Contains("crmp_numberformat") ? (string)sequence["crmp_numberformat"] : "0000";

                                    int startnum = sequence.Contains("crmp_startingnumber") ? (int)sequence["crmp_startingnumber"] : 1;
                                    int currenum = sequence.Contains("crmp_currentnumber") ? (int)sequence["crmp_currentnumber"] : startnum;

                                    string retval = currenum.ToString(numfmt);
                                    if (prefix != "")
                                    {
                                        retval = prefix + retval;
                                    }

                                    if (suffix != "")
                                    {
                                        retval = retval + suffix;
                                    }

                                    tracing.Trace("About to update Target");
                                    Entity nw = new Entity(entity.LogicalName);
                                    nw.Id = entity.Id;
                                    nw[attribute] = retval;
                                    service.Update(nw);

                                    tracing.Trace("About to update sequence");
                                    sequence["crmp_currentnumber"] = currenum + 1;
                                    service.Update(sequence);

                                    tracing.Trace("Updated");
                                }
                                tracing.Trace("Exiting try block");
                            }
                            catch (Exception ex)
                            {
                                tracing.Trace("Unhandled exception: " + ex.Message);
                            }
                        }
                    }
                    else
                    {
                        tracing.Trace("Attribute to write sequence to was not found or empty");
                    }
                }
            }
            if (EnforcedDebugging.Get<bool>(activity) == true)
            {
                throw new Exception("ENFORCED DEBUGGING");
            }
        }
    }
}


