# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

	1. Import CRMP Sequence Generator managed solution in to an org instance
	2. Navigate to Sequence generator Entity -Go To - Settings - Sequence Generator
	3. Now create a record in Sequence Generator Entity for the required Entity you would like to create New ID
	4. Open the unmanaged solution you are working on
	5. Go to the Processes
	6. Add a new Workflow process
	7.  Uncheck - Run this workflow in the background 
	8. Check - Record is created
	9. Scope Organisation
	10. Create a step to run crmp.sequencegenerator.Workflows
	11. In properties - Set to False
	12. Save and Activate
	13. Now when a new record is created in the Entity, a new ID will be generated


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact